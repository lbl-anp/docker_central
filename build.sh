#!/bin/bash

rootdir="$(cd "$(dirname "${0}" )" >/dev/null 2>&1 && pwd)"

if [ -z "${1}" ]; then
    echo "Require at least one argument, the image to build"
fi

echo "WARNING: This script is only intended for development, it will mess with locally installed images that might be relevant for other applications"

image=$1
architecture=${image##*_}
image=${image%_*}
case "$architecture" in
    arm64v8);;
    x86);;
    l4t);;
    melodic.l4t);;
    noetic.arm64v8);;
    noetic.x86);;
    *)
      echo "Unknown architecture '${architecture}'"
      exit 1
      ;;
esac

cd "${rootdir}/images/${image}"

docker buildx build -f "Dockerfile.${architecture}" -t "registry.gitlab.com/lbl-anp/docker_central/${image}_${architecture}" .
docker tag "registry.gitlab.com/lbl-anp/docker_central/${image}_${architecture}" "registry.gitlab.com/lbl-anp/docker_central/${image}"
