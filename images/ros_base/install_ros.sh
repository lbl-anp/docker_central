# setup timezone and packages
echo 'America/Los_Angeles' > /etc/timezone
rm -f /etc/localtime
ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime
apt-get update
apt-get install -yq --no-install-recommends \
    tzdata \
    dirmngr \
    gnupg2 \
    build-essential \
    ninja-build \
    git \
    ptpd \
    python3 \
    python3-dev \
    python-is-python3 \
    python-dev-is-python3 \
    python3-pip \
    python3-vcstools
update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1
pip3 install --user --no-cache-dir --force pip

# Install ROS base distribution
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
echo "deb http://packages.ros.org/ros/ubuntu focal main" > /etc/apt/sources.list.d/ros1-latest.list
apt-get update
apt-get install -yq --no-install-recommends \
    ros-noetic-ros-base \
    ros-${ROS_DISTRO}-robot-state-publisher \
    python3-rosdep \
    wget
rosdep init
rosdep update --rosdistro $ROS_DISTRO

# # Add helper scripts
if [ ! -f "/wsutil.sh" ]; then
    wget -O /wsutil.sh https://gitlab.com/lbl-anp/docker_central/-/raw/main/images/ros_base/wsutil.sh
fi
if [ ! -f "/entrypoint.sh" ]; then
    wget -O /entrypoint.sh https://gitlab.com/lbl-anp/docker_central/-/raw/main/images/ros_base/entrypoint.sh
fi

# Setup workspace
chmod a+x /wsutil.sh /entrypoint.sh
/wsutil.sh --add_to_entrypoint --setup_subpath="setup.bash" /opt/ros/${ROS_DISTRO}
/wsutil.sh --init /workspace/ros

# clean up
rm -rf /var/lib/apt/lists/*
