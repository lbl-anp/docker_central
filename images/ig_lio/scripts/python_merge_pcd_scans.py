import open3d as o3d
import os
import argparse

# check the current working directory
print("Current working directory: ", os.getcwd())
print("Changing working directory to the data folder ...")
print("Testing IO for point cloud ...")


parser = argparse.ArgumentParser()
parser.add_argument(
    "-f",
    "--folder",
    type=str,
    help="include '/' at the end or else it will not work",
    required=True,
)
args = parser.parse_args()
folder = args.folder

if not os.path.exists(folder):
    print("Folder does not exist")
    exit(0)
pcds = []
print("Reading files from the folder ...")

for file in os.listdir(folder):
    if file.endswith(".pcd"):
        pcds.append(file)

print("Saving concatenated point cloud ...")
pd = o3d.geometry.PointCloud()
for file in pcds:
    # sort the pcds based on the timestamp
    pcds.sort(key=lambda x: x.split(".")[0])
    print("Reading file: ", file)
    pcd_temp = o3d.io.read_point_cloud(folder + file)
    pd += pcd_temp

# save the point cloud to a ply file
o3d.io.write_point_cloud(folder + "data.bag_points.ply", pd)
print("Point cloud saved successfully as data.bag_points.ply")
